import psycopg2
import json

relationnal_connection_data = {}

with open("./conf.json", "rt") as f:
    relationnal_connection_data = json.load(f)
    relationnal_connection_data = relationnal_connection_data["postgres"]

with psycopg2.connect(**relationnal_connection_data) as connection:
    with connection.cursor() as cursor:
        cursor.execute("""
        DROP TABLE IF EXISTS line CASCADE
        """)
        cursor.execute("""
        DROP TABLE IF EXISTS stop CASCADE
        """)
        cursor.execute("""
        DROP TABLE IF EXISTS line_stop CASCADE
        """)


    with connection.cursor() as cursor:
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS line (
            id VARCHAR(256) PRIMARY KEY,
            name VARCHAR(256)
        );
        """)

        cursor.execute("""
        CREATE TABLE IF NOT EXISTS stop (
            id VARCHAR(256) PRIMARY KEY,
            name VARCHAR(256),
            lon VARCHAR(256),
            lat VARCHAR(256)
        );
        """)

        cursor.execute("""
        CREATE TABLE IF NOT EXISTS desserte (
            id INTEGER PRIMARY KEY,
            line_id VARCHAR(256),
            stop_id VARCHAR(256),
            dest VARCHAR(256),
            codeparcours VARCHAR(256),
            ordre INTEGER
        );
        """)

        cursor.execute("SELECT * FROM pg_catalog.pg_tables;")
        rows = cursor.fetchall()
        print("tables", "line" in [r[1] for r in rows])