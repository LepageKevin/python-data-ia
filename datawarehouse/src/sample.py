import bonobo
import requests
import psycopg2
import json
import redis



# Fichier qui set la BDD
# r = requests.get('https://data.angers.fr/api/records/1.0/search/?dataset=horaires-theoriques-et-arrets-du-reseau-irigo-gtfs&facet=stop_id&facet=stop_name&facet=wheelchair_boarding&rows=-1')

relation_connection_data = {}
relation_connection_data_postgres = {}
relation_connection_data_tile38 = {}

with open("./conf.json", "rt") as f:
    relation_connection_data = json.load(f)
    relation_connection_data_postgres = relation_connection_data['postgres']
    relation_connection_data_tile38 = relation_connection_data['tile38']

ROWS = -1

def fetch_desserte_data():
    #f permet de mettre un paramètre
    url = f"https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-topologie-dessertes&facet=codeparcours&facet=mnemoligne&facet=nomligne&facet=dest&facet=mnemoarret&facet=nomarret&facet=numarret&rows={ROWS}"
    response = requests.get(url)
    for record in response.json().get("records"):
        yield record

def get_interesting_fields(record):
    result = {
        "line_id": record["fields"]["mnemoligne"],
        "line_name": record["fields"]["nomligne"],
        "stop_id": record["fields"]["mnemoarret"],
        "stop_name": record["fields"]["nomarret"],
        "stop_code": record["fields"]["numarret"],
        "stop_lat": record["fields"]["coordonnees"][0],
        "stop_lon": record["fields"]["coordonnees"][1],
        "desserte_id": record["fields"]["iddesserte"],
        "desserte_dest": record["fields"]["dest"],
        "desserte_codeparcours": record["fields"]["codeparcours"],
        "desserte_ordre": record["fields"]["ordre"]
    }
    yield result

def insert_line(record):
    # permet d'éviter d'écrire le commit de la connexion
    with psycopg2.connect(**relation_connection_data_postgres) as connection:
        with connection.cursor() as cursor:
            query = """ INSERT INTO line (id, name) VALUES (%s, %s) ON CONFLICT (id) DO UPDATE SET name=EXCLUDED.name"""
            cursor.execute(query, (record["line_id"], record["line_name"]))
            print("insert line", record["line_id"])
    yield record

def insert_stop(record):
    # permet d'éviter d'écrire le commit de la connexion
    with psycopg2.connect(**relation_connection_data_postgres) as connection:
        with connection.cursor() as cursor:
            query = """ INSERT INTO stop (id, name, lon, lat) VALUES (%s, %s, %s, %s) ON CONFLICT (id) DO UPDATE SET name=EXCLUDED.name"""
            cursor.execute(query, (record["stop_id"], record["stop_name"], record["stop_lon"], record["stop_lat"]))
            print("insert stop", record["stop_id"])
    yield record

def insert_stop_tile38(record):
    client = redis.Redis(host=relation_connection_data_tile38["host"], port=relation_connection_data_tile38["port"])
    # insert data
    result = client.execute_command(
        'SETCHAN',
        relation_connection_data_tile38['fleet'],
        'nearby',
        relation_connection_data_tile38['collection'],
        'fence',
        'POINT',
        record['stop_lon'],
        record['stop_lat'],
        '200'
        )
    print("add geofence in tile38", record['stop_id'])
    yield record

def insert_desserte(record):
     # permet d'éviter d'écrire le commit de la connexion
    if "desserte_id" in record:
        with psycopg2.connect(**relation_connection_data_postgres) as connection:
            with connection.cursor() as cursor:
                query = """ INSERT INTO desserte (id, line_id, stop_id, dest, codeparcours, ordre) VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT (id) DO UPDATE SET line_id=EXCLUDED.line_id, stop_id=EXCLUDED.stop_id"""
                cursor.execute(query, (record["desserte_id"], record["line_id"], record["stop_id"], record["desserte_dest"], record["desserte_codeparcours"], record["desserte_ordre"]))
                print("insert desserte", record["desserte_id"])
    yield record

def transform_line(record):
    result = record["line_id"] + ";" + record["line_name"]
    yield result

def transform_stop(record):
    result = record["stop_id"] + ";" + record["stop_name"] + ";" + str(record["stop_lon"]) + ";" + str(record["stop_lat"])
    yield result

def transform_desserte(record):
    result = str(record["desserte_id"]) + ";" + record["line_id"] + ";" + record["stop_id"] + ";" + record["desserte_dest"] + ";" + record["desserte_codeparcours"] + ";" + str(record["desserte_ordre"])
    yield result

graph = bonobo.Graph()
graph.add_chain(
    fetch_desserte_data,
    get_interesting_fields,
    insert_line,
    insert_stop,
    insert_desserte
)

graph.add_chain(
    transform_line,
    bonobo.CsvWriter('init_db_line.csv'),
    _input=get_interesting_fields
)

graph.add_chain(
    transform_stop,
    bonobo.CsvWriter('init_db_stop.csv'),
    _input=get_interesting_fields
)

graph.add_chain(
    transform_desserte,
    bonobo.CsvWriter('init_db_desserte.csv'),
    _input=get_interesting_fields
)

# Création d'une deuxième chaîne pour faire le traîtement de l'insertion relationnel et Time Serial en parallèle
graph.add_chain(
    insert_stop_tile38,
    _input=insert_stop
)

bonobo.run(graph)