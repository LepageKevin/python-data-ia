import psycopg2
import json

relationnal_connection_data = {}

with open("./conf.json", "rt") as f:
    relationnal_connection_data = json.load(f)
    relationnal_connection_data = relationnal_connection_data["postgres"]

with psycopg2.connect(**relationnal_connection_data) as connection:
    with connection.cursor() as cursor:
        cursor.execute("""
        DROP TABLE IF EXISTS position CASCADE
        """)


    with connection.cursor() as cursor:
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS position (
            timestamp TIMESTAMPTZ NOT NULL,
            vehicule_id INTEGER NOT NULL,
            lon REAL NOT NULL,
            lat REAL NOT NULL,
            type VARCHAR(256),
            state VARCHAR(256),
            desserte_id INTEGER,
            estimated_desserte_schedule VARCHAR(256),
            estimated_desserte_delay INTEGER
        );
        """)
        # Transformation de la table pour recevoir les données temporelles
        cursor.execute("""SELECT create_hypertable('position', 'timestamp')""")