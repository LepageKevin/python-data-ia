import requests
import psycopg2
import redis
import time
import bonobo
import json


graph = bonobo.Graph()

ROWS = -1

relation_connection_data = {}
relation_connection_data_postgres = {}
relation_connection_data_tile38 = {}

with open("./conf.json", "rt") as f:
    relation_connection_data = json.load(f)
    relation_connection_data_postgres = relation_connection_data['postgres']
    relation_connection_data_tile38 = relation_connection_data['tile38']

def fetch_data():
    while 1:
        url = f"https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-position-tr&rows={ROWS}"
        response = requests.get(url)
        for record in response.json().get("records"):
            yield record
        # 5 minutes
        print("wait 5 minutes")
        time.sleep(300)

def get_interesting_fields(record):
    result = {
        "idvh": record["fields"]["idvh"],
        "lat": record["fields"]["coordonnees"][0],
        "lon": record["fields"]["coordonnees"][1],
        "type": record["fields"]["type"],
        "etat": record["fields"]["etat"],
        "desserte_id": record["fields"]["iddesserte"],
        "harret": record["fields"]["harret"],
        "ecart": record["fields"]["ecart"]
    }
    yield result

#Stocker bdd temporel open ts db cassandra, timesqueldb ou autre (tte les 5 min)
def insert_timeserie(record):
    # permet d'éviter d'écrire le commit de la connexion
    with psycopg2.connect(**relation_connection_data_postgres) as connection:
        with connection.cursor() as cursor:
            query = """ INSERT INTO position (timestamp, vehicule_id, lat, lon, type, state, desserte_id, estimated_desserte_schedule, estimated_desserte_delay) VALUES (NOW(), %s, %s, %s, %s, %s, %s, %s, %s) """
            cursor.execute(query, (record["idvh"], record["lat"], record["lon"], record["type"], record["etat"], record["desserte_id"], record["harret"], record["ecart"]))
            print("insert position ", record["idvh"])
    yield record

def insert_tile38(record):
    client = redis.Redis(host=relation_connection_data_tile38["host"], port=relation_connection_data_tile38["port"])
    collection_name = relation_connection_data_tile38["collection"]
    id_vehicule = record['fields']['idvh']
    lat = record["fields"]["coordonnees"][1]
    lon = record["fields"]["coordonnees"][0]
    cmd = [
        "SET",
        collection_name,
        f"vh_{id_vehicule}",
        "point",
        lat,
        lon
    ]
    result = client.execute_command(*cmd)
    print(f"vehicule {id_vehicule} moved to {lat}, {lon}")
    yield record


graph.add_chain(fetch_data)
graph.add_chain(get_interesting_fields, insert_timeserie, _input=fetch_data)
graph.add_chain(insert_tile38, _input=fetch_data)

bonobo.run(graph)